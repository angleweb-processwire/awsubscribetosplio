<?php

namespace ProcessWire;

// Source : https://developer.splio.com/

class AwSubscribeToSplio extends WireData implements Module, ConfigurableModule
{
    public const API_URL_AUTH = 'https://api.splio.com/authenticate';
    public const API_URL_CONTACT = 'https://api.splio.com/data/contacts';
    public const API_URL_LISTS = 'https://api.splio.com/data/v1/lists';

    public static function getModuleInfo()
    {
        return [
            'title'   => 'Subscribe to Splio',
            'summary' => 'Subscribe a user in your Splio mailing audience.',
            'version' => '0.0.1',
            'author'  => 'Angle Web',
            'href'    => '',
            'icon'    => 'address-card',
            'autoload' => false,
            'singular' => false
        ];
    }

    public static function getDefaults()
    {
        return array(
            "double_opt_in" => "checked",
        );
    }

    /**
     * Format post fields in JSON
     *
     * @param array $postFields
     *
     * @return string Json string
     */
    private static function formatPostFields(array $postFields)
    {
        return json_encode($postFields);
    }

    /**
     * Send a request to Splio with post fields parameters
     *
     * @param string $apiUrl
     * @param array $postFields
     * @param string|null $authToken
     * @param string $method GET or POST. Default: POST
     *
     * @return false|string False if error. JSON String on success
     */
    private static function sendApiRequest(string $apiUrl, array $postFields, ?string $authToken = null, string $method = 'POST')
    {
        $curlClient = curl_init();

        if (is_array($postFields)) {
            $postFields = self::formatPostFields($postFields);
        } elseif (is_string($postFields)) {
            $postFields = $postFields;
        } else {
            wire()->warning('Post fields parameter is not a string or an array: ' . get_class($postFields), Notice::log); // Log warning in Processwire backend
            return false;
        }

        bd($postFields, 'POST FIELDS');

        curl_setopt($curlClient, CURLOPT_URL, $apiUrl);
        curl_setopt($curlClient, CURLOPT_RETURNTRANSFER, true);

        if ($method == 'POST') {
            curl_setopt($curlClient, CURLOPT_POST, true);
            curl_setopt($curlClient, CURLOPT_POSTFIELDS, $postFields);
        } elseif ($method == 'GET') {
            curl_setopt($curlClient, CURLOPT_CUSTOMREQUEST, 'GET');
        }

        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-Type: application/json';
        if (!is_null($authToken)) {
            $headers[] = 'Authorization: Bearer ' . $authToken;
        }

        curl_setopt($curlClient, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($curlClient);

        if (curl_errno($curlClient)) {
            wire()->warning('Exception when calling SPLIO API: ' . curl_error($curlClient), Notice::log); // Log warning in Processwire backend
            curl_close($curlClient);
            return false;
        } else {
            curl_close($curlClient);
            return $result;
        }
    }

    /**
     * Get API Token authentification
     *
     * @return false|string False if error. String with Token if success.
     */
    private function getAuthToken()
    {
        $postFields = [
            'api_key' => $this->api_key
        ];

        $result = self::sendApiRequest(self::API_URL_AUTH, $postFields);

        if ($result !== false) {
            $jsonToken = json_decode($result);

            if (property_exists($jsonToken, 'token')) {
                return $jsonToken->token;
            } else {
                wire()->warning('Error to get the auth token from request result: ' . strval($result), Notice::log); // Log warning in Processwire backend
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Create contact from email with double opt-in process
     *
     * @param string $authToken
     * @param string $contactEmail
     * @param string $messageId Id of the message that must be sent. This message must contain a link to $confirmUrl$ .
     * @param array|null $listIds
     * @param array|null $contactAttributes
     *
     * @return false|string False if error. JSON String on success
     */
    public static function createContactWithDoubleOptIn(string $authToken, string $contactEmail, string $messageId, ?array $listIds = null, ?array $contactAttributes = null)
    {
        $postFields = [
            'double_optin' => [
                'message' => $messageId
            ],
            'email' => $contactEmail,
            'language' => 'fr'
        ];

        if (!is_null($listIds)) {
            $postFields['lists'] = $listIds;
        }

        if (!is_null($contactAttributes)) {
            $postFields['custom_fields'] = $contactAttributes;
        }

        $result = self::sendApiRequest(self::API_URL_CONTACT, $postFields, $authToken);

        if ($result !== false) {
            $json = json_decode($result);

            if (property_exists($json, 'errors')) {
                wire()->warning('Exception when calling SPLIO To create double opt-in contact:' . $json->errors[0]->error_description, Notice::log); // Log warning in Processwire backend
                return false;
            } else {
                return $result;
            }
        } else {
            wire()->warning('Exception when calling SPLIO To create double opt-in contact', Notice::log); // Log warning in Processwire backend
            return false;
        }
    }

    /**
     * Create contact from email
     *
     * @param string $authToken
     * @param string $contactEmail
     * @param array|null $listIds
     * @param array|null $contactAttributes
     *
     * @return false|string False if error. JSON String on success
     */
    public static function createContact(string $authToken, string $contactEmail, ?array $listIds = null, ?array $contactAttributes = null)
    {
        $postFields = [
            'email' => $contactEmail,
            'language' => 'fr',
        ];

        if (!is_null($listIds)) {
            $postFields['lists'] = $listIds;
        }

        if (!is_null($contactAttributes)) {
            $postFields['custom_fields'] = $contactAttributes;
        }

        $result = self::sendApiRequest(self::API_URL_CONTACT, $postFields, $authToken);

        if ($result !== false) {
            $json = json_decode($result);

            if (property_exists($json, 'errors')) {
                wire()->warning('Exception when calling SPLIO To create contact:' . $json->errors[0]->error_description, Notice::log); // Log warning in Processwire backend
                return false;
            } else {
                return $result;
            }
        } else {
            wire()->warning('Exception when calling SPLIO To create contact', Notice::log); // Log warning in Processwire backend
            return false;
        }
    }

    /**
     * Get contact list details from ID
     *
     * @param integer $listId Id of the list
     *
     * @return bool|stdClass False if error, list standard class if list found
     */
    public function getListDetails(int $listId)
    {
        // Get Auth Token
        $authToken = $this->getAuthToken();

        $result = self::sendApiRequest(self::API_URL_LISTS, [], $authToken, 'GET');

        if ($result !== false) {
            $lists = json_decode($result);
            if ($lists->elements) {
                foreach ($lists->elements as $list) {
                    if ($list->id && $list->id == $listId) {
                        return $list;
                    }
                }
            }
        }

        wire()->warning('Exception when calling SPLIO To create contact', Notice::log); // Log warning in Processwire backend
        return false;
    }

    /**
     * Subscribe an email to Splio
     *
     * @param string $contactEmail
     * @param array|null $contactAttributes
     *
     * @return void
     */
    public function subscribe(string $contactEmail, array $contactAttributes = null)
    {
        // Get Auth Token
        $authToken = $this->getAuthToken();

        if ($authToken !== false) {
            if ($this->double_opt_in) {
                return $this->createContactWithDoubleOptIn($authToken, $contactEmail, $this->template_id, [['id' => intval($this->default_list)]], [$contactAttributes]);
            } else {
                return $this->createContact($authToken, $contactEmail, [['id' => intval($this->default_list)]], [$contactAttributes]);
            }
        } else {
            return false;
        }
    }

    public static function getModuleConfigInputfields(array $data)
    {
        $defaults = self::getDefaults();
        $data = array_merge($defaults, $data);
        $wrap = new InputfieldWrapper();
        $form = wire('modules')->get('InputfieldFieldset');
        $form->label = __('Send In Blue Configuration');
        $form->notes = __('Check out the README, if you have troubles to find the data for the fields above.');
        $form->collapsed = wire('session')->sendinblue_test_settings ? Inputfield::collapsedNo : Inputfield::collapsedPopulated;

        $inputfields = [
            'api_key' => __('API Key'),
            'default_list' => __('Default list id'),
            'template_id' => __('Default email id for double opt-in')
        ];
        $checkboxfields = [
            'double_opt_in' => __('Use double opt-in (Recommended)'),
        ];
        foreach ($inputfields as $name => $label) {
            $f = wire('modules')->get('InputfieldText');
            $f->attr('name', $name);
            $f->label = $label;
            $f->required = true;
            $f->columnWidth = 50;
            if (isset($data[$name]))
                $f->attr('value', $data[$name]);
            $form->add($f);
        }
        foreach ($checkboxfields as $name => $label) {
            $f = wire('modules')->get("InputfieldCheckbox");
            $f->name = $name;
            $f->label = $label;
            $f->attr('checked', empty($data[$name]) ? '' : 'checked');
            $f->columnWidth = 100;
            $form->add($f);
        }

        // TEST SETTINGS ||>>>
        $f = wire('modules')->get('InputfieldCheckbox');
        $f->attr('name', '_sendinblue_test_settings');
        $f->label = __('Test your current settings!');
        $f->description = __('Check this box and press save, to test the API Key and default audience settings!');
        $f->attr('value', 1);
        $f->attr('checked', '');
        $f->icon = 'heartbeat';
        $f->columnWidth = wire('session')->sendinblue_test_settings ? 40 : 100;
        $form->add($f);

        if (wire('session')->sendinblue_test_settings) {
            wire('session')->remove('sendinblue_test_settings');
            $response = self::testSettings();
            $f = wire('modules')->get('InputfieldMarkup');
            $f->attr('name', '_test_response');
            $f->label = __('Test Response: ');
            $f->columnWidth = 60;
            $f->attr('value', $response);
            $form->add($f);
        } else if (wire('input')->post->_sendinblue_test_settings) {
            wire('session')->set('sendinblue_test_settings', 1);
        }
        // <<<|| TEST SETTINGS

        $wrap->add($form);
        return $wrap;
    }

    // static, so it can get called from static function getModuleConfigInputfields, but also from within your template files: $response = SubscribeToMailchimp::testSettings($listID);
    public static function testSettings(int $listId = null): string
    {
        /** @var AwSubscribeToSendinblue $splioModule */
        $splioModule = wire('modules')->get('AwSubscribeToSplio');

        if (is_null($listId)) {
            $listId = $splioModule->default_list;
        } else {
            $listId = $listId;
        }

        $listDetails = $splioModule->getListDetails($listId);

        if ($listDetails && property_exists($listDetails, 'id')) {
            $successPattern = '<li>%s <strong>%s</strong></li>';
            $output = '';

            if (isset($listDetails->name)) {
                $output .= sprintf($successPattern, __('Name:'), $listDetails->name);
            }

            if (isset($listDetails->id)) {
                $output .= sprintf($successPattern, __('ID:'), $listDetails->id);
            }

            if (!empty($output)) {
                $output = '<ul style="color:green;">' . $output . '</ul>';
            }

            return $output;
        } else {
            $errorPattern = '<p style="color:red;">%s %s</p>';
            return sprintf($errorPattern, __('Splio request not successful:'), 'List ID not found or server error');
        }
    }
}
